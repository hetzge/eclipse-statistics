package de.hetzge.eclipse.statistics.util;

public enum Traverse {
	Continue, Stop, DontGoDeeper
}