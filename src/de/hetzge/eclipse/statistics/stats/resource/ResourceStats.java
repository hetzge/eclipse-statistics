package de.hetzge.eclipse.statistics.stats.resource;

import java.util.Map;
import java.util.Set;

import de.hetzge.eclipse.statistics.eclipse.EclipseUtils;
import de.hetzge.eclipse.statistics.stats.BaseTypeCountStats;

public final class ResourceStats extends BaseTypeCountStats {
	public ResourceStats(Map<Integer, Integer> countByTypeId) {
		super(countByTypeId);
	}

	@Override
	protected Set<Integer> getAllTypeIds() {
		return EclipseUtils.resourceElementTypeIds();
	}

	@Override
	protected String toDisplay(Integer typeId) {
		return EclipseUtils.resourceElementDisplay(typeId);
	}
}