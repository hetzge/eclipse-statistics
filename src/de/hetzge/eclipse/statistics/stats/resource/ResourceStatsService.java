package de.hetzge.eclipse.statistics.stats.resource;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;

public class ResourceStatsService implements IResourceStatsService {

	@Override
	public ResourceStats scanResourceStats() throws CoreException {
		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final IWorkspaceRoot workspaceRoot = workspace.getRoot();

		return new ResourceStats(scanResourceStats(workspaceRoot));
	}

	private Map<Integer, Integer> scanResourceStats(IResource resource) throws CoreException {
		final Map<Integer, Integer> countByTypeId = new HashMap<>();
		resource.accept(resource1 -> {
			countByTypeId.put(resource1.getType(), countByTypeId.getOrDefault(resource1.getType(), 0) + 1);
			return true;
		});
		return countByTypeId;
	}
}
