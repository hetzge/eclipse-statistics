package de.hetzge.eclipse.statistics.stats.resource;

import org.eclipse.core.runtime.CoreException;

public interface IResourceStatsService {

	ResourceStats scanResourceStats() throws CoreException;

}