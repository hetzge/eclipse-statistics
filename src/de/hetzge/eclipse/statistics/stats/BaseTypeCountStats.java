package de.hetzge.eclipse.statistics.stats;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public abstract class BaseTypeCountStats implements Iterable<CountStat> {
	private final Map<Integer, Integer> countByTypeId;

	public BaseTypeCountStats(Map<Integer, Integer> countByTypeId) {
		this.countByTypeId = countByTypeId;
	}

	@Override
	public Iterator<CountStat> iterator() {
		return getAllTypeIds().stream().map(this::toStat).sorted().iterator();
	}

	private CountStat toStat(Integer typeId) {
		return new CountStat(typeId.toString(), toDisplay(typeId), countByTypeId.getOrDefault(typeId, 0));
	}

	protected abstract Set<Integer> getAllTypeIds();

	protected abstract String toDisplay(Integer typeId);
}