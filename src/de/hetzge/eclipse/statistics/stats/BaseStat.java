package de.hetzge.eclipse.statistics.stats;

public abstract class BaseStat implements Stat, Comparable<Stat> {
	
	@Override
	public int compareTo(Stat other) {
		return display().compareTo(other.display());
	}
	
}
