package de.hetzge.eclipse.statistics.stats.java;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaModelException;

import de.hetzge.eclipse.statistics.JavaElementFilter;
import de.hetzge.eclipse.statistics.eclipse.EclipseUtils;

public class JavaStatsService implements IJavaStatsService {

	public JavaStats scanJavaStats(IProgressMonitor monitor, JavaElementFilter filter) throws JavaModelException {
		final List<IJavaElement> elements = EclipseUtils.elements(filter);

		final SubMonitor elementsSubMonitor = SubMonitor.convert(monitor, elements.size());

		final Map<Integer, Integer> countByTypeId = new HashMap<>();
		for (IJavaElement element : elements) {
			elementsSubMonitor.split(1);
			countByTypeId.put(element.getElementType(), countByTypeId.getOrDefault(element.getElementType(), 0) + 1);
		}

		return new JavaStats(countByTypeId);
	}

}
