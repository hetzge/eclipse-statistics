package de.hetzge.eclipse.statistics.stats.java;

import java.util.Map;
import java.util.Set;

import de.hetzge.eclipse.statistics.eclipse.EclipseUtils;
import de.hetzge.eclipse.statistics.stats.BaseTypeCountStats;

public final class JavaStats extends BaseTypeCountStats {
	public JavaStats(Map<Integer, Integer> countByTypeId) {
		super(countByTypeId);
	}

	@Override
	protected Set<Integer> getAllTypeIds() {
		return EclipseUtils.javaElementTypeIds();
	}

	@Override
	protected String toDisplay(Integer typeId) {
		return EclipseUtils.javaElementDisplay(typeId);
	}
}