package de.hetzge.eclipse.statistics.stats.java;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.JavaModelException;

import de.hetzge.eclipse.statistics.JavaElementFilter;

public interface IJavaStatsService {

	JavaStats scanJavaStats(IProgressMonitor monitor, JavaElementFilter filter) throws JavaModelException;

}