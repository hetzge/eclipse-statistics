package de.hetzge.eclipse.statistics.stats.javacode;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.JavaModelException;

import de.hetzge.eclipse.statistics.JavaElementFilter;

public interface IJavaCodeStatsService {

	JavaCodeStats scanJavaCodeStats(IProgressMonitor monitor, JavaElementFilter filter) throws JavaModelException;

}