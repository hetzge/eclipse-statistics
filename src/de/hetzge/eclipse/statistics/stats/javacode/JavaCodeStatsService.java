package de.hetzge.eclipse.statistics.stats.javacode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;

import de.hetzge.eclipse.statistics.JavaElementFilter;
import de.hetzge.eclipse.statistics.eclipse.EclipseUtils;

public class JavaCodeStatsService implements IJavaCodeStatsService {

	@Override
	public JavaCodeStats scanJavaCodeStats(IProgressMonitor monitor, JavaElementFilter filter)
			throws JavaModelException {
		final List<ITypeRoot> typeRoots = EclipseUtils.typeRootElements(filter);
		final SubMonitor elementsSubMonitor = SubMonitor.convert(monitor, typeRoots.size());
		final CountByNodeTypeAstVisitor astVisitor = new CountByNodeTypeAstVisitor();

		final ASTParser parser = ASTParser.newParser(AST.JLS11);

		for (ITypeRoot typeRoot : typeRoots) {
			elementsSubMonitor.split(1);

			if (typeRoot.getSource() != null) {
				parser.setSource(typeRoot);
				parser.createAST(null).accept(astVisitor);
			}
		}

		return new JavaCodeStats(astVisitor.getCountByTypeId());
	}

	private class CountByNodeTypeAstVisitor extends ASTVisitor {

		private final Map<Integer, Integer> countByTypeId;

		public CountByNodeTypeAstVisitor() {
			this.countByTypeId = new HashMap<>();
		}

		@Override
		public void preVisit(ASTNode node) {
			final int nodeTypeId = node.getNodeType();
			countByTypeId.put(nodeTypeId, countByTypeId.getOrDefault(nodeTypeId, 0) + 1);
			super.preVisit(node);
		}

		public Map<Integer, Integer> getCountByTypeId() {
			return countByTypeId;
		}
	}
}
