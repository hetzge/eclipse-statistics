package de.hetzge.eclipse.statistics.stats.javacode;

import java.util.Map;
import java.util.Set;

import de.hetzge.eclipse.statistics.eclipse.EclipseUtils;
import de.hetzge.eclipse.statistics.stats.BaseTypeCountStats;

public final class JavaCodeStats extends BaseTypeCountStats {
	public JavaCodeStats(Map<Integer, Integer> countByTypeId) {
		super(countByTypeId);
	}

	@Override
	protected Set<Integer> getAllTypeIds() {
		return EclipseUtils.javaAstTypeIds();
	}

	@Override
	protected String toDisplay(Integer typeId) {
		return EclipseUtils.javaAstElementDisplay(typeId);
	}
}