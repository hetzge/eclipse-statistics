package de.hetzge.eclipse.statistics.stats;

import java.util.Objects;

public final class CountStat extends BaseStat {
	private final String key;
	private final String display;
	private final int value;

	public CountStat(String key, String display, int value) {
		this.key = key;
		this.display = display != null ? display : "";
		this.value = value;
	}

	@Override
	public String key() {
		return key;
	}

	@Override
	public String display() {
		return display;
	}

	@Override
	public String valueDisplay() {
		return String.valueOf(value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(key);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CountStat)) {
			return false;
		}
		CountStat other = (CountStat) obj;
		return Objects.equals(key, other.key);
	}
}