package de.hetzge.eclipse.statistics.stats;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public final class Stats implements Iterable<Stat> {

	private final Set<Stat> stats;

	public Stats() {
		this.stats = new HashSet<>();
	}

	@Override
	public Iterator<Stat> iterator() {
		return this.stats.stream().sorted().iterator();
	}

	public void set(CountStat stat) {
		this.stats.remove(stat);
		this.stats.add(stat);
	}
}
