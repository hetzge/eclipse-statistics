package de.hetzge.eclipse.statistics.stats;

public interface Stat {

	String key();

	String display();
	
	String valueDisplay();

}
