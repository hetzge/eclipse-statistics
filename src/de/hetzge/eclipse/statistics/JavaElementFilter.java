package de.hetzge.eclipse.statistics;

import java.util.List;

import org.eclipse.jdt.core.IJavaElement;

public final class JavaElementFilter {

	private final List<? extends IJavaElement> rootElements;
	private final boolean excludeExternalDependencies;

	public JavaElementFilter(List<? extends IJavaElement> rootElements, boolean excludeExternalDependencies) {
		this.rootElements = rootElements;
		this.excludeExternalDependencies = excludeExternalDependencies;
	}

	public List<? extends IJavaElement> getRootElements() {
		return rootElements;
	}

	public boolean isExcludeExternalDependencies() {
		return excludeExternalDependencies;
	}

}
