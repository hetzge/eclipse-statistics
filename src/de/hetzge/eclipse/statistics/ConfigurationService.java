package de.hetzge.eclipse.statistics;

import javax.inject.Inject;

import org.eclipse.core.commands.State;
import org.eclipse.ui.handlers.RegistryToggleState;
import org.eclipse.ui.internal.commands.CommandService;

public class ConfigurationService implements IConfigurationService {

	@Inject
	private CommandService commandService;

	@Override
	public boolean isExcludeDependencies() {
		final State state = commandService.getCommand("de.hetzge.eclipse.statistics.ExcludeDependenciesCommand")
				.getState(RegistryToggleState.STATE_ID);

		return state != null && Boolean.TRUE.equals(state.getValue());
	}

}
