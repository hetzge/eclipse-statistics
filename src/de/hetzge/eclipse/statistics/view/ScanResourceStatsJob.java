package de.hetzge.eclipse.statistics.view;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.ui.di.UISynchronize;

import de.hetzge.eclipse.statistics.eclipse.EclipseUtils;
import de.hetzge.eclipse.statistics.eclipse.MutexSchedulingRule;
import de.hetzge.eclipse.statistics.stats.resource.IResourceStatsService;
import de.hetzge.eclipse.statistics.stats.resource.ResourceStats;

public class ScanResourceStatsJob extends Job {

	private static final String NAME = "Scan resource stats";

	private final UISynchronize uiSynchronize;
	private final IResourceStatsService resourceStatsService;
	private final StatsTreeItem statsTreeItem;

	public ScanResourceStatsJob(UISynchronize uiSynchronize, IResourceStatsService resourceStatsService,
			StatsTreeItem statsTreeItem) {
		super(NAME);
		this.uiSynchronize = uiSynchronize;
		this.resourceStatsService = resourceStatsService;
		this.statsTreeItem = statsTreeItem;

		setRule(new MutexSchedulingRule(NAME));
		setPriority(Job.LONG);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		try {
			uiSynchronize.asyncExec(() -> {
				statsTreeItem.loading();
			});

			final ResourceStats resourceStats = resourceStatsService.scanResourceStats();
			uiSynchronize.asyncExec(() -> {
				statsTreeItem.setup(resourceStats);
			});

			return Status.OK_STATUS;
		} catch (CoreException e) {
			e.printStackTrace();

			uiSynchronize.asyncExec(() -> {
				statsTreeItem.error();
				EclipseUtils.showErrorMessageDialog("Failed to scan resource stats", e.getMessage());
			});

			return Status.CANCEL_STATUS;
		} finally {
			monitor.done();
		}
	}
}