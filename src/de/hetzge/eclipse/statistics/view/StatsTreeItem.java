package de.hetzge.eclipse.statistics.view;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

import de.hetzge.eclipse.statistics.stats.Stat;

public final class StatsTreeItem {
	private final String title;
	private final Tree tree;
	private final TreeItem item;
	private final Map<Stat, TreeItem> itemsByStat;

	public StatsTreeItem(Tree tree, String title) {
		this.tree = tree;
		this.title = title;
		this.item = new TreeItem(tree, SWT.NONE);
		this.itemsByStat = new HashMap<>();

		item.setText(new String[] { title, "" });
		item.setExpanded(true);
	}

	public void loading() {
		item.setText(new String[] { title, "loading..." });
	}

	public void error() {
		item.setText(new String[] { title, "error !!!" });
	}

	public void canceled() {
		item.setText(new String[] { title, "canceled !!!" });
	}

	public void setup(Iterable<? extends Stat> stats) {
		for (Stat stat : stats) {
			final TreeItem statItem = itemsByStat.computeIfAbsent(stat, key -> new TreeItem(item, SWT.NONE));
			statItem.setText(0, stat.display());
			statItem.setText(1, stat.valueDisplay());
		}

		item.setExpanded(true);

		for (int i = 0; i < tree.getColumns().length; i++) {
			final TreeColumn column = tree.getColumns()[i];
			column.pack();
		}

		item.setText(new String[] { title, "" });
	}
	
	public TreeItem treeItem() {
		return item;
	}
}