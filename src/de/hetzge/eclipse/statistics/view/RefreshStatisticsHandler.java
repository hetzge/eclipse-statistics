package de.hetzge.eclipse.statistics.view;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.ui.PlatformUI;

public class RefreshStatisticsHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		final IEventBroker broker = PlatformUI.getWorkbench().getService(IEventBroker.class);
		broker.send(StatisticsView.REFRESH_EVENT, new Object());

		return null;
	}
}
