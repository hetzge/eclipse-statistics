package de.hetzge.eclipse.statistics.view;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;

public class ExcludeDependenciesHandler extends AbstractHandler {
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		System.out.println("ExcludeDependenciesHandler");
		
		HandlerUtil.toggleCommandState(event.getCommand());
		
		return null;
	}
}
