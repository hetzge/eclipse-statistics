package de.hetzge.eclipse.statistics.view;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.ui.di.UISynchronize;

import de.hetzge.eclipse.statistics.eclipse.MutexSchedulingRule;
import de.hetzge.eclipse.statistics.stats.Stats;

public class ScanGeneralStatsJob extends Job {

	private final static String NAME = "Scan general stats";

	private final UISynchronize uiSynchronize;
	private final StatsTreeItem statsTreeItem;

	public ScanGeneralStatsJob(UISynchronize uiSynchronize, StatsTreeItem statsTreeItem) {
		super(NAME);
		this.uiSynchronize = uiSynchronize;
		this.statsTreeItem = statsTreeItem;

		setRule(new MutexSchedulingRule(NAME));
		setPriority(Job.LONG);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		try {
			uiSynchronize.asyncExec(() -> {
				statsTreeItem.loading();
			});

			final Stats stats = new Stats();
			uiSynchronize.asyncExec(() -> {
				statsTreeItem.setup(stats);
			});

			return Status.OK_STATUS;
		} finally {
			monitor.done();
		}
	}
}