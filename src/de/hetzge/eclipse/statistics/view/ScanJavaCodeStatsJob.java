package de.hetzge.eclipse.statistics.view;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.jdt.core.IJavaProject;

import de.hetzge.eclipse.statistics.IConfigurationService;
import de.hetzge.eclipse.statistics.JavaElementFilter;
import de.hetzge.eclipse.statistics.eclipse.EclipseUtils;
import de.hetzge.eclipse.statistics.eclipse.MutexSchedulingRule;
import de.hetzge.eclipse.statistics.stats.javacode.IJavaCodeStatsService;
import de.hetzge.eclipse.statistics.stats.javacode.JavaCodeStats;

public class ScanJavaCodeStatsJob extends Job {

	private static final String NAME = "Scan java code stats";

	private final UISynchronize uiSynchronize;
	private final IConfigurationService configurationService;
	private final IJavaCodeStatsService javaCodeStatsService;
	private final StatsTreeItem statsTreeItem;

	public ScanJavaCodeStatsJob(UISynchronize uiSynchronize, IConfigurationService configurationService,
			IJavaCodeStatsService javaCodeStatsService, StatsTreeItem statsTreeItem) {
		super(NAME);
		this.uiSynchronize = uiSynchronize;
		this.configurationService = configurationService;
		this.javaCodeStatsService = javaCodeStatsService;
		this.statsTreeItem = statsTreeItem;

		setRule(new MutexSchedulingRule(NAME));
		setPriority(Job.LONG);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		try {
			uiSynchronize.asyncExec(() -> {
				statsTreeItem.loading();
			});

			final List<IJavaProject> workspaceJavaProjects = EclipseUtils.getWorkspaceJavaProjects();
			final JavaElementFilter javaElementFilter = new JavaElementFilter(workspaceJavaProjects,
					configurationService.isExcludeDependencies());
			final JavaCodeStats javaCodeStats = javaCodeStatsService.scanJavaCodeStats(monitor, javaElementFilter);
			uiSynchronize.asyncExec(() -> {
				statsTreeItem.setup(javaCodeStats);
			});

			return Status.OK_STATUS;
		} catch (OperationCanceledException e) {
			uiSynchronize.asyncExec(() -> {
				statsTreeItem.canceled();
			});

			return Status.CANCEL_STATUS;
		} catch (CoreException e) {
			e.printStackTrace();

			uiSynchronize.asyncExec(() -> {
				statsTreeItem.error();
				EclipseUtils.showErrorMessageDialog("Failed to scan java code stats", e.getMessage());
			});

			return Status.CANCEL_STATUS;
		} finally {
			monitor.done();
		}
	}
}