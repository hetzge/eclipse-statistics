package de.hetzge.eclipse.statistics.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;

public final class StatsTree {
	private final Tree tree;
	private final StatsTreeItem generalStatsTreeItem;
	private final StatsTreeItem javaStatsTreeItem;
	private final StatsTreeItem javaCodeStatsTreeItem;
	private final StatsTreeItem resourceStatsTreeItem;

	public StatsTree(Composite parent) {
		this.tree = new Tree(parent, SWT.NONE);
		tree.setLinesVisible(true);
		tree.setHeaderVisible(true);

		final TreeColumn keyColumn = new TreeColumn(tree, SWT.LEFT);
		keyColumn.setText("Key");
		final TreeColumn valueColumn = new TreeColumn(tree, SWT.LEFT);
		valueColumn.setText("Value");

		this.generalStatsTreeItem = new StatsTreeItem(tree, "Workspace stats");
		this.javaStatsTreeItem = new StatsTreeItem(tree, "Java stats");
		this.javaCodeStatsTreeItem = new StatsTreeItem(tree, "Java code stats");
		this.resourceStatsTreeItem = new StatsTreeItem(tree, "Resource stats");
	}

	public StatsTreeItem generalStatsTreeItem() {
		return generalStatsTreeItem;
	}

	public StatsTreeItem javaStatsTreeItem() {
		return javaStatsTreeItem;
	}

	public StatsTreeItem javaCodeStatsTreeItem() {
		return javaCodeStatsTreeItem;
	}

	public StatsTreeItem resourceStatsTreeItem() {
		return resourceStatsTreeItem;
	}
}