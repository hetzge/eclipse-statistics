package de.hetzge.eclipse.statistics.view;

import javax.inject.Inject;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import de.hetzge.eclipse.statistics.ConfigurationService;
import de.hetzge.eclipse.statistics.stats.java.JavaStatsService;
import de.hetzge.eclipse.statistics.stats.javacode.JavaCodeStatsService;
import de.hetzge.eclipse.statistics.stats.resource.ResourceStatsService;

public class StatisticsView extends ViewPart {

	public static final String REFRESH_EVENT = "HETZGE/STATISTICS/REFRESH";

	@Inject
	private UISynchronize uiSynchronize;

	@Inject
	private ConfigurationService configurationService;

	@Inject
	private JavaStatsService javaStatsService;

	@Inject
	private JavaCodeStatsService javaCodeStatsService;

	@Inject
	private ResourceStatsService resourceStatsService;

	private Composite parent;
	private StatsTree statsTree;

	@Override
	public void createPartControl(Composite parent) {
		this.parent = parent;
		this.statsTree = new StatsTree(parent);
	}

	@Inject
	@Optional
	private void onRefreshEvent(@UIEventTopic(REFRESH_EVENT) Object payload) {
		execute(new ScanGeneralStatsJob(uiSynchronize, statsTree.generalStatsTreeItem()));
		execute(new ScanJavaStatsJob(uiSynchronize, configurationService, javaStatsService,
				statsTree.javaStatsTreeItem()));
		execute(new ScanJavaCodeStatsJob(uiSynchronize, configurationService, javaCodeStatsService,
				statsTree.javaCodeStatsTreeItem()));
		execute(new ScanResourceStatsJob(uiSynchronize, resourceStatsService, statsTree.resourceStatsTreeItem()));
	}

	@Override
	public void setFocus() {
		// ignore
	}

	private void execute(Job job) {
		cancelJob(job.getName());
		job.schedule();
	}

	private void cancelJob(String jobName) {
		for (Job job : Job.getJobManager().find(null)) {
			if (job.getName().equals(jobName)) {
				job.cancel();
			}
		}
	}
}
