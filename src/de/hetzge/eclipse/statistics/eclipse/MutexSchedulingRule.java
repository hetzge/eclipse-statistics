package de.hetzge.eclipse.statistics.eclipse;

import org.eclipse.core.runtime.jobs.ISchedulingRule;

public class MutexSchedulingRule implements ISchedulingRule {

	private final Object mutexKey;

	public MutexSchedulingRule(Object mutexKey) {
		this.mutexKey = mutexKey;
	}

	@Override
	public boolean isConflicting(ISchedulingRule rule) {
		return rule instanceof MutexSchedulingRule && ((MutexSchedulingRule) rule).mutexKey.equals(mutexKey);
	}

	@Override
	public boolean contains(ISchedulingRule rule) {
		return isConflicting(rule);
	}
}