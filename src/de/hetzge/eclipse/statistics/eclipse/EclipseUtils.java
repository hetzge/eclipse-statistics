package de.hetzge.eclipse.statistics.eclipse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IParent;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.internal.core.JavaModel;
import org.eclipse.jdt.internal.core.JavaModelManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import de.hetzge.eclipse.statistics.JavaElementFilter;
import de.hetzge.eclipse.statistics.util.Traverse;

public final class EclipseUtils {

	private static Map<Integer, String> RESOURCE_ELEMENT_DISPLAY_BY_TYPE_ID = new HashMap<Integer, String>() {
		{
			put(IResource.FILE, "File");
			put(IResource.FOLDER, "Folder");
			put(IResource.PROJECT, "Project");
			put(IResource.ROOT, "Root");
		}
	};

	private static Map<Integer, String> JAVA_ELEMENT_DISPLAY_BY_TYPE_ID = new HashMap<Integer, String>() {
		{
			put(IJavaElement.JAVA_MODEL, "Java model");
			put(IJavaElement.JAVA_PROJECT, "Java project");
			put(IJavaElement.PACKAGE_FRAGMENT_ROOT, "Package fragment root");
			put(IJavaElement.PACKAGE_FRAGMENT, "Package fragment");
			put(IJavaElement.COMPILATION_UNIT, "Compilation unit");
			put(IJavaElement.CLASS_FILE, "Class file");
			put(IJavaElement.TYPE, "Type");
			put(IJavaElement.FIELD, "Field");
			put(IJavaElement.METHOD, "Method");
			put(IJavaElement.INITIALIZER, "Initializer");
			put(IJavaElement.PACKAGE_DECLARATION, "Package declaration");
			put(IJavaElement.IMPORT_CONTAINER, "Import container");
			put(IJavaElement.IMPORT_DECLARATION, "Import declaration");
			put(IJavaElement.LOCAL_VARIABLE, "Local variable");
			put(IJavaElement.TYPE_PARAMETER, "Type parameter");
			put(IJavaElement.ANNOTATION, "Annotation");
			put(IJavaElement.JAVA_MODULE, "Java module");
		}
	};

	private static Map<Integer, String> JAVA_AST_DISPLAY_BY_TYPE_ID = new HashMap<Integer, String>() {
		{
			put(ASTNode.ANONYMOUS_CLASS_DECLARATION, "Anonymous class decleration");
			put(ASTNode.ARRAY_ACCESS, "Array access");
			put(ASTNode.ARRAY_CREATION, "Array creation");
			put(ASTNode.ARRAY_INITIALIZER, "Array initializer");
			put(ASTNode.ARRAY_TYPE, "Array type");
			put(ASTNode.ASSERT_STATEMENT, "Assert statement");
			put(ASTNode.ASSIGNMENT, "Assignment");
			put(ASTNode.BLOCK, "Block");
			put(ASTNode.BOOLEAN_LITERAL, "Boolean literal");
			put(ASTNode.BREAK_STATEMENT, "Break statement");
			put(ASTNode.CAST_EXPRESSION, "Cast expression");
			put(ASTNode.CATCH_CLAUSE, "Catch clause");
			put(ASTNode.CHARACTER_LITERAL, "Character literal");
			put(ASTNode.CLASS_INSTANCE_CREATION, "Class instance creation");
			put(ASTNode.COMPILATION_UNIT, "Compilation unit");
			put(ASTNode.CONDITIONAL_EXPRESSION, "Conditional expression");
			put(ASTNode.CONSTRUCTOR_INVOCATION, "Constructor invocation");
			put(ASTNode.CONTINUE_STATEMENT, "Continue statement");
			put(ASTNode.DO_STATEMENT, "Do statement");
			put(ASTNode.EMPTY_STATEMENT, "Empty statement");
			put(ASTNode.EXPRESSION_STATEMENT, "Expression statement");
			put(ASTNode.FIELD_ACCESS, "Field access");
			put(ASTNode.FIELD_DECLARATION, "Field declaration");
			put(ASTNode.FOR_STATEMENT, "For statement");
			put(ASTNode.IF_STATEMENT, "If statement");
			put(ASTNode.IMPORT_DECLARATION, "Import declaration");
			put(ASTNode.INFIX_EXPRESSION, "Infix expression");
			put(ASTNode.INITIALIZER, "Initializer");
			put(ASTNode.JAVADOC, "Javadoc");
			put(ASTNode.LABELED_STATEMENT, "Labeled statement");
			put(ASTNode.METHOD_DECLARATION, "Method declaration");
			put(ASTNode.METHOD_INVOCATION, "Method invocation");
			put(ASTNode.NULL_LITERAL, "Null literal");
			put(ASTNode.NUMBER_LITERAL, "Number literal");
			put(ASTNode.PACKAGE_DECLARATION, "Package declaration");
			put(ASTNode.PARENTHESIZED_EXPRESSION, "Parenthesized expression");
			put(ASTNode.POSTFIX_EXPRESSION, "Postfix expression");
			put(ASTNode.PREFIX_EXPRESSION, "Prefix expression");
			put(ASTNode.PRIMITIVE_TYPE, "Primitive type");
			put(ASTNode.QUALIFIED_NAME, "Qualified name");
			put(ASTNode.RETURN_STATEMENT, "Return statement");
			put(ASTNode.SIMPLE_NAME, "Simple name");
			put(ASTNode.SIMPLE_TYPE, "Simple type");
			put(ASTNode.SINGLE_VARIABLE_DECLARATION, "Single variable declaration");
			put(ASTNode.STRING_LITERAL, "String literal");
			put(ASTNode.SUPER_CONSTRUCTOR_INVOCATION, "Super constructor invocation");
			put(ASTNode.SUPER_FIELD_ACCESS, "Super field access");
			put(ASTNode.SUPER_METHOD_INVOCATION, "Super method invocation");
			put(ASTNode.SWITCH_CASE, "Switch case");
			put(ASTNode.SWITCH_STATEMENT, "Switch statement");
			put(ASTNode.SYNCHRONIZED_STATEMENT, "Synchronized statement");
			put(ASTNode.THIS_EXPRESSION, "This expression");
			put(ASTNode.THROW_STATEMENT, "Throw statement");
			put(ASTNode.TRY_STATEMENT, "Try statement");
			put(ASTNode.TYPE_DECLARATION, "Type declaration");
			put(ASTNode.TYPE_DECLARATION_STATEMENT, "Type declaration statement");
			put(ASTNode.TYPE_LITERAL, "Type literal");
			put(ASTNode.VARIABLE_DECLARATION_EXPRESSION, "Variable declaration expression");
			put(ASTNode.VARIABLE_DECLARATION_FRAGMENT, "Variable declaration fragment");
			put(ASTNode.VARIABLE_DECLARATION_STATEMENT, "Variable declaration statement");
			put(ASTNode.WHILE_STATEMENT, "While statement");
			put(ASTNode.INSTANCEOF_EXPRESSION, "InstanceOf expression");
			put(ASTNode.LINE_COMMENT, "Line comment");
			put(ASTNode.BLOCK_COMMENT, "Block comment");
			put(ASTNode.TAG_ELEMENT, "Tag element");
			put(ASTNode.TEXT_ELEMENT, "Text element");
			put(ASTNode.MEMBER_REF, "Member reference");
			put(ASTNode.METHOD_REF, "Method reference");
			put(ASTNode.METHOD_REF_PARAMETER, "Method reference parameter");
			put(ASTNode.ENHANCED_FOR_STATEMENT, "Enhanced for statement");
			put(ASTNode.ENUM_DECLARATION, "Enum declaration");
			put(ASTNode.ENUM_CONSTANT_DECLARATION, "Enum constant declaration");
			put(ASTNode.TYPE_PARAMETER, "Type parameter");
			put(ASTNode.PARAMETERIZED_TYPE, "Parameterized type");
			put(ASTNode.QUALIFIED_TYPE, "Qualified type");
			put(ASTNode.WILDCARD_TYPE, "Wildcard type");
			put(ASTNode.NORMAL_ANNOTATION, "Normal annotation");
			put(ASTNode.MARKER_ANNOTATION, "Marker annotation");
			put(ASTNode.SINGLE_MEMBER_ANNOTATION, "Single member annotation");
			put(ASTNode.MEMBER_VALUE_PAIR, "Member value pair");
			put(ASTNode.ANNOTATION_TYPE_DECLARATION, "Annotation type declaration");
			put(ASTNode.ANNOTATION_TYPE_MEMBER_DECLARATION, "Annotation type memer declaration");
			put(ASTNode.MODIFIER, "Modifier");
			put(ASTNode.UNION_TYPE, "Union type");
			put(ASTNode.DIMENSION, "Dimension");
			put(ASTNode.LAMBDA_EXPRESSION, "Lambda expression");
			put(ASTNode.INTERSECTION_TYPE, "Intersection type");
			put(ASTNode.NAME_QUALIFIED_TYPE, "Name qualified type");
			put(ASTNode.CREATION_REFERENCE, "Creation reference");
			put(ASTNode.EXPRESSION_METHOD_REFERENCE, "Expression method reference");
			put(ASTNode.SUPER_METHOD_REFERENCE, "Super method reference");
			put(ASTNode.TYPE_METHOD_REFERENCE, "Type method reference");
			put(ASTNode.MODULE_DECLARATION, "Module declaration");
			put(ASTNode.REQUIRES_DIRECTIVE, "Requires directive");
			put(ASTNode.EXPORTS_DIRECTIVE, "Exports directive");
			put(ASTNode.OPENS_DIRECTIVE, "Opens directive");
			put(ASTNode.USES_DIRECTIVE, "Uses directive");
			put(ASTNode.PROVIDES_DIRECTIVE, "Provides directive");
			put(ASTNode.MODULE_MODIFIER, "Module modifier");
		}
	};

	public static Set<Integer> resourceElementTypeIds() {
		return RESOURCE_ELEMENT_DISPLAY_BY_TYPE_ID.keySet();
	}

	public static Set<Integer> javaElementTypeIds() {
		return JAVA_ELEMENT_DISPLAY_BY_TYPE_ID.keySet();
	}

	public static Set<Integer> javaAstTypeIds() {
		return JAVA_AST_DISPLAY_BY_TYPE_ID.keySet();
	}

	public static String resourceElementDisplay(int typeId) {
		return RESOURCE_ELEMENT_DISPLAY_BY_TYPE_ID.getOrDefault(typeId, "");
	}

	public static String javaElementDisplay(int typeId) {
		return JAVA_ELEMENT_DISPLAY_BY_TYPE_ID.getOrDefault(typeId, "");
	}

	public static String javaAstElementDisplay(int typeId) {
		return JAVA_AST_DISPLAY_BY_TYPE_ID.getOrDefault(typeId, "");
	}

	public static void showErrorMessageDialog(String title, String error) {
		EclipseUtils.showMessageDialog(title, "Error: " + error);
	}

	public static void showMessageDialog(String title, String message) {
		final Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		MessageDialog.openInformation(shell, title, message);
	}

	// --------------------------

	public static List<ITypeRoot> typeRootElements(JavaElementFilter filter) throws JavaModelException {
		return elements(filter).stream().filter(ITypeRoot.class::isInstance).map(ITypeRoot.class::cast)
				.collect(Collectors.toList());
	}

	public static List<IJavaElement> elements(JavaElementFilter filter) throws JavaModelException {
		final List<IJavaElement> elements = new ArrayList<>();
		final List<? extends IJavaElement> rootElements = filter.getRootElements();
		traverse(rootElements, element -> {
			if (filter.isExcludeExternalDependencies() && isDependency(element)) {
				return Traverse.DontGoDeeper;
			} else {
				elements.add(element);
				return Traverse.Continue;
			}
		});

		return elements;
	}

	private static Traverse traverse(Iterable<? extends IJavaElement> elements,
			Function<IJavaElement, Traverse> visitor) throws JavaModelException {
		loop: for (IJavaElement element : elements) {
			final Traverse traverse = visitor.apply(element);

			if (traverse == Traverse.Continue) {
				if (element instanceof IParent) {
					final IParent parent = (IParent) element;
					final Traverse innerTraverse = traverse(Arrays.asList(parent.getChildren()), visitor);
					if (innerTraverse == Traverse.Stop) {
						return Traverse.Stop;
					}
				}
				continue loop;
			} else if (traverse == Traverse.Stop) {
				return Traverse.Stop;
			} else if (traverse == Traverse.DontGoDeeper) {
				continue loop;
			}
		}
		return Traverse.Continue;
	}

	public static List<IJavaProject> getWorkspaceJavaProjects() {
		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final IWorkspaceRoot workspaceRoot = workspace.getRoot();

		final JavaModelManager javaModelManager = JavaModelManager.getJavaModelManager();
		final JavaModel javaModel = javaModelManager.getJavaModel();
		return Arrays.stream(workspaceRoot.getProjects()).map(project -> javaModel.getJavaProject(project))
				.filter(Objects::nonNull).collect(Collectors.toList());
	}

	private static boolean isDependency(IJavaElement element) {
		if (element instanceof IPackageFragmentRoot) {
			final IPackageFragmentRoot packageFragmentRoot = (IPackageFragmentRoot) element;
			return packageFragmentRoot.isReadOnly();
		} else {
			return false;
		}
	}

}